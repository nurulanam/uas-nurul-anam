import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../screen/Home';
import SignUp from '../screen/SignUp';
import Login from '../screen/Login';
import Utama from '../screen/Utama';

function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Home'
        screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Detail" component={SignUp} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Utama" component={Utama}/>

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;