import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput
  } from 'react-native'
  import React from 'react'
  
  export default function Login({navigation}) {
    return (
      <View style={{
        backgroundColor: '#94b3e8',
        flex: 1
      }}>
        <Image source={require('./Image/Mentahan.png')} style={{
          width: 720,
          height: 500,
        }} />
        <Text style={{
          fontSize: 40,
          color: '#ffffff',
          marginStart: 50,
          marginTop: 100,
          fontWeight: 'bold',
  
        }}>USERNAME</Text>
        <TextInput style={{
          borderBottomWidth: 1,
          borderColor: '#ffffff',
          marginHorizontal: 50,
          color: '#ffffff',
          fontSize: 20
        }}>
  
        </TextInput >
        <Text style={{
          fontSize: 40,
          color: '#ffffff',
          marginStart: 50,
          marginTop: 50,
          fontWeight: 'bold',
        }}>PASSWORD</Text>
        <TextInput style={{
          borderBottomWidth: 1,
          borderColor: '#ffffff',
          marginHorizontal: 50,
          color: '#ffffff',
          fontSize: 20
        }}>
  
        </TextInput>
        <Text style={{
          color: '#ffffff',
          fontSize: 15,
          marginTop: 30,
          marginStart: 520
        }}>FORGET PASSWORD ?</Text>
  
        <TouchableOpacity onPress={()=>navigation.navigate('Utama')} style={{
          flex: 0.3,
          backgroundColor: '#ffffff',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 50,
          borderRadius: 30,
          marginHorizontal: 50,
          marginVertical: 150
        }}>
          <Text style={{
            fontSize: 40,
            fontWeight: 'bold'
          }}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    )
  }