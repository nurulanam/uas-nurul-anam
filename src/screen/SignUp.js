import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput
  } from 'react-native'
  import React from 'react'
  
  export default function SignUp({ navigation }) {
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#94b3e8',
      }}>
        <Image source={require('./Image/Mentahan.png')} style={{
          height: 500,
          width: 720,
        }} />
        <View style={{
          height: 500,
          width: 600,
          backgroundColor: '#ffffff',
          marginTop: 100,
          borderRadius: 30,
          marginStart: 60
        }}>
          <Text style={{
            color: '#94b3e8',
            fontSize: 40,
            fontWeight: 'bold',
            marginTop: 40,
            marginStart: 40
          }}>USERNAME</Text>
          <TextInput style={{
            borderBottomWidth: 3,
            marginHorizontal: 40,
            borderColor: '#94b3e8',
            fontSize: 20,
            color: '#94b3e8'
          }}></TextInput>
          <Text style={{
            color: '#94b3e8',
            fontSize: 40,
            fontWeight: 'bold',
            marginTop: 40,
            marginStart: 40
          }}>PASSWORD 1</Text>
          <TextInput style={{
            borderBottomWidth: 3,
            marginHorizontal: 40,
            borderColor: '#94b3e8',
            fontSize: 20,
            color: '#94b3e8'
          }}></TextInput>
          <Text style={{
            color: '#94b3e8',
            fontSize: 40,
            fontWeight: 'bold',
            marginTop: 40,
            marginStart: 40
          }}>PASSWORD 2</Text>
          <TextInput style={{
            borderBottomWidth: 3,
            marginHorizontal: 40,
            borderColor: '#94b3e8',
            fontSize: 20,
            color: '#94b3e8'
          }}></TextInput>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Utama')} style={{
          height: 80,
          width: 200,
          backgroundColor: '#ffffff',
          marginTop: 70,
          alignItems: 'center',
          justifyContent: 'center',
          marginStart: 260,
          borderRadius: 10
        }}>
          <Text style={{
            fontSize: 40,
            fontWeight: 'bold',
            color: '#0078AA',
          }}>DAFTAR</Text>
        </TouchableOpacity>
  
  
      </View>
    )
  }