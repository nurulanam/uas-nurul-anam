import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native'
import React from 'react'
import Home from './Home'

export default function Utama({ navigation }) {
  return (
    <View style={{
      backgroundColor: '#1f1f1f',
      flex: 1
    }}>
      <View style={{
        backgroundColor: '#ffffff',
        flex: 0.03,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
      }}>
        <TouchableOpacity>
          <Text style={{
            fontSize: 20,
          }}>
            HOME
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={{
            fontSize: 20,
            marginStart: 40
          }}>
            TRANSAKSI
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={{
            fontSize: 20,
            marginStart: 40
          }}>
            INFO
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={{
            fontSize: 20,
            marginStart: 40
          }}>
            KATEGORI
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={{
            fontSize: 20,
            marginStart: 40
          }}>
            ABOUT
          </Text>
        </TouchableOpacity>
      </View>
      <Image source={require('./Image/background.png')} style={{
        width: 700,
        height: 180
      }} />
      <Text style={{
        color: '#ffffff',
        fontSize: 50,
        fontWeight: 'bold',
        marginStart: 240,
        paddingTop: 90
      }}>waktunya</Text>
      <Text style={{
        color: '#ffffff',
        fontSize: 50,
        fontWeight: 'bold',
        marginStart: 240,
        marginTop: 50
      }}>mepet pak</Text>
      <Text style={{
        color: '#ffffff',
        fontSize: 50,
        fontWeight: 'bold',
        marginStart: 240,
        marginTop: 50
      }}>dak selesai</Text>

      <TouchableOpacity onPress={()=>navigation.navigate('Home')} style={{
        backgroundColor: '#ffffff',
        flex: 0.1,
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 250,
        borderRadius: 10,
        borderColor: '#ffc500',
        borderWidth: 5,
      }}>
        <Text style={{
          color: '#1f1f1f',
          fontSize: 40,
          fontWeight: 'bold'
        }}>BAIK</Text>
      </TouchableOpacity>
    </View>
  )
}