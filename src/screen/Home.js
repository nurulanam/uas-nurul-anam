import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Image
} from 'react-native'
import React from 'react'
import Navigation from '../navigation/navigation'

export default function Home({ navigation }) {
    return (
        <View style={{
            alignItems: 'center',
            backgroundColor: '#94b3e8',
            flex: 1,
        }}>
            <Image source={require('./Image/Mentahan.png')} style={{
                width: 800,
                height: 500,
            }} />
            <TouchableOpacity onPress={() => navigation.navigate('Detail')}>
                <View style={{
                    backgroundColor: '#ffffff',
                    height: 80,
                    width: 600,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    marginTop: 200
                }}>
                    <Text style={{
                        color: '#0078AA',
                        fontSize: 50,
                        fontWeight: 'bold'
                    }}>
                        SIGN UP
                    </Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <View style={{
                    backgroundColor: '#ffffff',
                    height: 80,
                    width: 600,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 70,
                    borderRadius: 20
                }}>
                    <Text style={{
                        color: '#0078AA',
                        fontSize: 50,
                        fontWeight: 'bold'
                    }}>
                        LOGIN
                    </Text>
                </View>
            </TouchableOpacity>


        </View>
    )
}